/** Show scopes **/
app.component('prmSearchBarAfter', {
  bindings: { parentCtrl: '<' },
  controller: 'SearchBarAfterController'
});

app.controller('SearchBarAfterController', ['angularLoad', function(angularLoad) {
  var vm = this;

  this.$onInit = function(){
    {
      vm.parentCtrl.showTabsAndScopes = true;
    }
  };
}]);
/** end scopes code **/

